import java.util.*;
public class Gravitacija {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
       // System.out.println("OIS je zakon!");
        double visina = sc.nextDouble();
        double pospesek = izracun(visina);
        izpis(pospesek, visina);
    }
    
    public static double izracun(double v) {
        double C = 6.674 * Math.pow(10, -11);
        double M = 5.972 * Math.pow(10, 24);
        double r = 6.371 * Math.pow(10, 6); 
        return ((C*M)/((r+v) * (r+v)));
    }
    public static void izpis(double pospesek, double visina) {
        System.out.printf("%f%n%f", visina, pospesek);
    }  
}
